# Hot-Backup for Hetzner-Servers with LVM-snapshots

This setup utilizes LVM-snapshots to get a consistent live-backup from a [Hetzner](https:// hetzner.com) server.


Requirements

* This setup is in use on Ubuntu. Not tested on CentOS.
* The `cifs-utils`-package needs to be installed

## fstab


```fstab
proc /proc proc defaults 0 0
/dev/md/0 none swap sw 0 0
/dev/md/1 /boot ext3 defaults 0 0
/dev/vgmain/lvmain / ext4 defaults 0 0
//<username>.your-backup.de/backup /mnt/backup-server       cifs    iocharset=utf8,rw,credentials=/etc/backup-credentials.txt,uid=0,gid=0,file_mode=0660,dir_mode=0770 0       0

```

`dev/vgmain/lvmain` is the partition of which the backup is created. There should be space available for a snapshot in the volume group.


The backup-medium is mount via CIFS (see [the documentation at hetzner.com](https://wiki.hetzner.de/index.php/Backup/en#SAMBA.2FCIFS)). The file `/etc/backup-credentials.txt` must have mode `0600` and the contents

```
username=...
password=...
```

## Going from plain SoftRAID-1 (mdadm) to SoftRAID-1 with LVM on the root partition.

The [Hetzner](hetzner.com) system I started with did not have lvm enabled by default. I am not sure if that was my decision or just the template 
Have a look at ["Setting up LVM on a system with only a RAID1"](raid1-to-lvm.md)

## Setup 

* Install backup2l and the cifs-utils.
  `sudo apt-get install -y backup2l cifs-utils`
* Make sure the fstab-line of the `/mnt/backup-server` (see above) is part of your fstab.
* The file `/etc/backup-credentials.txt` should contain username and passwort of the backup-store (see https://wiki.hetzner.de/index.php/Backup/en#SAMBA.2FCIFS for details) and have `0600`-permissions.
* Copy the file [backup2l.conf](backup2l.conf) to `/etc/backup2l.conf`
* Copy the file [run-backup.sh](run-backup.sh) to `/etc/cron.daily/zz-run-backup`

## Process

The following actions will be executed by `run-backup.sh`

* Create a new LVM snapshot of `/dev/vgmain/lvmain`
* Mount this snapshot on `/media/lvmain-snapshot`
* **Create a backup** by running `backup2l` chrooted to `media/lvmain-snapshot`
* Remove all mounts below (or equal) `/media/lvmain-snapshot`
* Remove the LVM snapshot

The last two steps are implemented in a `trap` so that they are also executed, if the backup is aborted. They are also executed before everything else to cleanup left-over snapshot volumes in case of previous failures.


