#!/bin/bash

# Load settings from the same directory as the run-backup-script
SOURCE_LV=/dev/vgmain/lvmain
SNAPSHOT_MOUNT=/media/lvmain-snapshot


# Computed variables
SNAPSHOT_LV=${SOURCE_LV}-snapshot


set -e

# Cleanup hooks
function cleanup {
  echo "Removing ${SNAPSHOT_LV} in one second"
  sleep 1
  # Remove all mounts mounted on the snapshot
  for i in $( mount | grep -o "${SNAPSHOT_MOUNT}[^ ]*" | sort -r ) ; do
    umount "$i"
  done
  # remove the snapshotgi
  lvremove -f "${SNAPSHOT_LV}"
}

trap cleanup EXIT

# Cleanup if snapshot device already exists
if [ -e "${SNAPSHOT_LV}" ] ; then
    echo "Snapshot ${SNAPSHOT_LV} already exists."
    cleanup
fi

# Create directory
mkdir -p "${SNAPSHOT_MOUNT}"
# Create lvm snapshot of root file system
lvcreate -L10G -s -n "${SNAPSHOT_LV}" "${SOURCE_LV}"

# mount snapshot
mount "${SNAPSHOT_LV}" "${SNAPSHOT_MOUNT}"

# run backup-manager
chroot "${SNAPSHOT_MOUNT}" backup2l --backup


# Cleanup is done by the "trap cleanup EXIT"